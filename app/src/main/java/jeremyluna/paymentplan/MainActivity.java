package jeremyluna.paymentplan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText loan_amount, interest_amount;
    private TextView payment5, payment10, payment15, payment20, payment25, payment30;
    float principle, interest_rate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loan_amount = findViewById(R.id.loan_text);
        interest_amount = findViewById(R.id.interest_text);

        payment5 = findViewById(R.id.yearAmount5);
        payment10 = findViewById(R.id.yearAmount10);
        payment15 = findViewById(R.id.yearAmount15);
        payment20 = findViewById(R.id.yearAmount20);
        payment25 = findViewById(R.id.yearAmount25);
        payment30 = findViewById(R.id.yearAmount30);
    }


    public void updatePlan(View view) {
        try{
            principle = Float.parseFloat(loan_amount.getText().toString());
            interest_rate = Float.parseFloat(interest_amount.getText().toString());
            float tmp_total;

            tmp_total = principle;
            for (int i = 0; i < 5; i++){
                tmp_total *= (1.0 + (interest_rate/100));
            }
            payment5.setText(String.format("%.2f", tmp_total/(5*12) ));

            tmp_total = principle;
            for (int i = 0; i < 10; i++){
                tmp_total *= (1.0 + (interest_rate/100));
            }
            payment10.setText(String.format("%.2f", tmp_total/(10*12) ));

            tmp_total = principle;
            for (int i = 0; i < 15; i++){
                tmp_total *= (1.0 + (interest_rate/100));
            }
            payment15.setText(String.format("%.2f", tmp_total/(15*12) ));

            tmp_total = principle;
            for (int i = 0; i < 20; i++){
                tmp_total *= (1.0 + (interest_rate/100));
            }
            payment20.setText(String.format("%.2f", tmp_total/(20*12) ));

            tmp_total = principle;
            for (int i = 0; i < 25; i++){
                tmp_total *= (1.0 + (interest_rate/100));
            }
            payment25.setText(String.format("%.2f", tmp_total/(25*12) ));

            tmp_total = principle;
            for (int i = 0; i < 30; i++){
                tmp_total *= (1.0 + (interest_rate/100));
            }
            payment30.setText(String.format("%.2f", tmp_total/(30*12) ));
        }
        catch(Exception e){
            //nothing
        }
    }
}
